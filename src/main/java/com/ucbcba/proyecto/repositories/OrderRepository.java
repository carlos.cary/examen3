package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order,Integer>{
}
