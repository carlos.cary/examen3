package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Direction;
import com.ucbcba.proyecto.entities.Zona;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface ZonaRepository extends CrudRepository<Zona,Integer> {
}
