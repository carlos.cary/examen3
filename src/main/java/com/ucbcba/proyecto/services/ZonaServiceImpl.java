package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Zona;
import com.ucbcba.proyecto.repositories.ZonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ZonaServiceImpl implements ZonaService{

    private ZonaRepository zonaRepository;

    @Autowired
    @Qualifier(value = "zonaRepository")
    public void setProductRepository(ZonaRepository zonaRepository){
        this.zonaRepository = zonaRepository;
    }

    @Override
    public Iterable<Zona> listAllZona() {
        return zonaRepository.findAll();
    }

    @Override
    public Zona getZonaById(Integer id) {
        return zonaRepository.findOne(id);
    }

    @Override
    public Zona saveZona(Zona zona) {
        return zonaRepository.save(zona);
    }
}
