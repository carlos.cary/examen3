package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Direction;


public interface DirectionService {

    Iterable<Direction> listAllDirection();

    Direction getDirectionById(Integer id);

    Direction saveDirection(Direction direction);

    void deleteDirection(Integer id);
}
