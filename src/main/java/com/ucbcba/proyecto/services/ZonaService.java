package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Zona;

public interface ZonaService {
    Iterable<Zona> listAllZona();

    Zona getZonaById(Integer id);

    Zona saveZona(Zona zona);
}
